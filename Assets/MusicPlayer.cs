﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicPlayer : MonoBehaviour
{
  // Start is called before the first frame update

  void Awake()
  {
    DontDestroyOnLoad(this);
  }

  void Start()
  {
    Invoke("loadFirstScene", 2f);
  }

  void loadFirstScene()
  {
    SceneManager.LoadScene(1);
  }

}
