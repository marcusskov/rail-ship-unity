﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Ship : MonoBehaviour
{
  [Tooltip("In ms^-1")] [SerializeField] float xSpeed = 15f;
  [Tooltip("In ms^-1")] [SerializeField] float ySpeed = 15f;

  void Start()
  {

  }

  void FixedUpdate()
  {
    float xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
    float yThrow = CrossPlatformInputManager.GetAxis("Vertical");

    transform.localPosition = new Vector3(
      Mathf.Clamp(offsetPosition(xThrow, xSpeed, transform.localPosition.x), -5.5f, 5.5f),
      Mathf.Clamp(offsetPosition(yThrow, ySpeed, transform.localPosition.y), -3.4f, 3.5f),
      transform.localPosition.z
    );
  }

  float offsetPosition(float posThrow, float speed, float transformsLocal)
  {
    float offSet = posThrow * speed * Time.deltaTime;
    return transformsLocal + offSet;
  }
}
